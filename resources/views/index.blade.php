<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="https://use.fontawesome.com/5c5f5e49d6.js"></script>
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Index</title>
</head>
<body id="myPage" data-spy="scroll" data-offset="60">

    <div id="preloader">
		<div id="loader">
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="lading"></div>
		</div>
    </div>

    <div id="banner">
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="logo wow fadeInDown animated" data-wow-duration="3s">
                        <h2 style="font-size: 30px;"><strong>Tiens</strong> Beauty</h2> </div>
                    </div>
                    <div class="col-md-3 callus">
                        <a href="tel:(0800)123-456-789"><small>Call us now:</small> (085) 695-398-738</a>
                    </div>
                </div>
            </div>
        </header>
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 mar-botm-304px">
                    <div class="banner-text wow fadeInRightBig animated" data-wow-duration="3s">
                        <h1><span>New Skin Face For you</span>
                        <br>
                        100% Natural Origin Products</h1>
                        <div class="clearfix"></div>
                        <div class="col-md-8 nopadding">
                            <button class="btn btn-default btn-shopnow text-uppercase">Shop Now</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div id="work">
        <div class="container">
            <div class="row">
                <div class="work_main text-center  col-sm-12">
                    <h2 class="wow fadeInUp animated" data-wow-duration="2s">Top Featured Products</h2>
                </div>
                <div class="clearfix"></div>
                <div class="work_base_block">
                    <div class="work_block col-sm-3">
                        <div class="work-img"><img src="images/produk-1.png" alt="image"></div>
                        <p class="wow fadeInDown" data-wow-duration="1s">Product Name<small>Rp 43K</small></p>
                    </div>
                    <div class="work_block col-sm-3">
                        <div class="work-img"><img src="images/produk-2.png" alt="image"></div>
                        <p class="wow fadeInDown" data-wow-duration="1s">Product Name<small>Rp 87K</small></p>
                    </div>
                    <div class="work_block col-sm-3">
                        <div class="work-img"><img src="images/produk-3.png" alt="image"></div>
                        <p class="wow fadeInDown" data-wow-duration="1s">Product Name<small>Rp 76K</small></p>
                    </div>
                    <div class="work_block col-sm-3">
                        <div class="work-img"><img src="images/produk-4.png" alt="image"></div>
                        <p class="wow fadeInDown" data-wow-duration="1s">Product Name<small>Rp 65K</small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="learn">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6 ">
                    <div class="learn-main text-left">
                        <h2 class="wow fadeInUp animated" data-wow-duration="2s">How It Works</h2>
                    </div>
                    <div class="learn-text">
                        <p class="wow fadeInUp animated">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            <br> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                            <br> veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea com-
                            <br>modo consequat.
                            <br>
                            <br>
                            <br> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <div class="row">
                            <div class="col-md-5 ">
                                <a href="#work" class="btn btn-default btn-submit">shop now</a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="services">
        <div class="container">
            <div class="row">
                <div class="services_main text-center col-sm-12">
                    <h2 class="wow fadeInUp animated" data-wow-duration="2s">FEATURE PRODUCTS</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="services-section-1">
                        <ul>
                            <li class="text-right">
                                <div class="number wow fadeIn text-right animated" data-wow-duration="2s">
                                    <i class="fa fa-pagelines" aria-hidden="true"></i>
                                </div>
                                <div class="points-1">
                                    <h4>Fresh Product</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim </p>
                                </div>
                            </li>
                            <li class="text-right">
                                <div class="number wow fadeIn" data-wow-duration="2s">
                                    <i class="fa fa-flask" aria-hidden="true"></i>
                                </div>
                                <div class="points-1">
                                    <h4>Pure Ingredients</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="services-img text-center wow pulse animated">
                        <img src="images/service-img.png" alt="image">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="services-section-2">
                        <ul>
                            <li>
                                <div class="number wow fadeIn animated" data-wow-duration="2s">
                                    <i class="fa fa-car" aria-hidden="true"></i>
                                </div>
                                <div class="points-2">
                                    <h4>Fast Delivery</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim </p>
                                </div>
                            </li>
                            <li>
                                <div class="number wow fadeIn " data-wow-duration="2s">
                                    <i class="fa fa-inbox" aria-hidden="true"></i>
                                </div>
                                <div class="points-2">
                                    <h4>Each Kind</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="subscribe">
        <div class="container">
            <div class="row">
                <div class="subscribe_main text-center col-sm-12">
                    <h2 class="wow fadeInUp animated" data-wow-duration="2s">Have Questions?</h2>
                    <div class="clearfix"></div>
                    <p>Feel free To Contact Us 24/7 Days A Week From 9.00 Am To 6 Pm</p>
                    <div class="clearfix"></div>
                    <h2 class="wow fadeInUp animated" data-wow-duration="2s">(085) 695-398-738</h2>
                    <div class="clearfix"></div>
                    <p>Or Get An App On</p>
                    <div class="clearfix"></div>
                    <a href="#myPage" class="btn btn-default btn-submit text-uppercase">Shop Now</a>
                </div>
            </div>
        </div>
    </div>

    <div id="screen">
        <div class="container">
            <div class="row">
                <div class="col-md-6 ">
                    <div class="screen-main text-left">
                        <h2 class="wow fadeInUp animated" data-wow-duration="2s">App Screenshots</h2>
                        <p class="wow fadeInUp animated">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            <br>
                            <br>
                            <br> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div class="screen-text">
                        <div class="row">
                            <div class="col-md-5 ">
                                <a href="#work" class="btn btn-default btn-submit">shop now</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="screen-video wow fadeInLeftBig animated">
                        <img src="images/screen-short.png" alt="icon">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="testimonial">
        <div class="row">
            <div class="col-md-8 col-center m-auto">
                <h2>Testimonials</h2>
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Carousel indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for carousel items -->
                    <div class="carousel-inner">
                        <div class="item carousel-item active">
                            <div class="img-box"><img src="images/testi-img1.png" alt=""></div>
                            <p class="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                            <p class="overview"><b>@imamaf</b></p>
                        </div>
                        <div class="item carousel-item">
                            <div class="img-box"><img src="images/testi-img1.png" alt=""></div>
                            <p class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Utmtc tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio.</p>
                            <p class="overview"><b>@imamaf</b></p>
                        </div>
                        <div class="item carousel-item">
                            <div class="img-box"><img src="images/testi-img1.png" alt=""></div>
                            <p class="testimonial">Phasellus vitae suscipit justo. Mauris pharetra feugiat ante id lacinia. Etiam faucibus mauris id tempor egestas. Duis luctus turpis at accumsan tincidunt. Phasellus risus risus, volutpat vel tellus ac, tincidunt fringilla massa. Etiam hendrerit dolor eget rutrum.</p>
                            <p class="overview"><b>@imamaf</b></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="copyRight">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="footer-detail">
                        <h3>Follow us on:
                            <a href="#"><i aria-hidden="true" class="fa fa-facebook"></i></a>
                            <a href="#"><i aria-hidden="true" class="fa fa-twitter"></i></a>
                            <a href="#"><i aria-hidden="true" class="fa fa-google-plus"></i></a>
                        </h3>
                    </div>
                </div>
                <div class="col-sm-6 ">
                    <p>© Copyright 2019 Beauty Product Tiens. Developed by <a target="_blank" href="#">Friday.com</a></p>
                </div>
            </div>
        </div>
    </div>

</body>
</html>

<script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/jQuery.scrollSpeed.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
